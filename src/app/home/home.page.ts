import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ApiService } from './-api-service.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  items: any[] = [];
  items2: any[] = [];
  ti: any[] = [];
  form: FormGroup = new FormGroup({
    selected: new FormControl(null)
  });

  constructor(private route: Router, private _apiService: ApiService) {

  }

  forms(){
    this.route.navigate(['/form']);
  }

  async loadDados(){
    const response = await fetch('../../assets/data/equipamentos.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

    const dados = await response.json();

    return dados;
  }


  async inputChanged($event, $tipo){
    //$event.target.value: Caracteres digitados no input

    //Quando digitar um dado no input será feita o processo de buscar os dados para recomendação
    if($event.target.value !== ''){
      const dadosJson = await this.loadDados();

      if($tipo === 'Brand'){
        const value = $event.target.value;
        if (value.legth <= 0){
          this.items = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.equipamentos.length -1; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.ti.indexOf(dadosJson.equipamentos[i].Brand) < 0){
            //console.log(users.equipamentos[i].Category);
            this.ti[x] = dadosJson.equipamentos[i].Brand;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.items = this.ti.filter(
          ta => ta.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        );

          //console.log(this.items);
      }else if($tipo === 'Category'){
        const value2 = $event.target.value;
        if (value2.legth <= 0){
          this.items2 = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.equipamentos.length -1; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.ti.indexOf(dadosJson.equipamentos[i].Category) < 0){
            //console.log(users.equipamentos[i].Category);
            this.ti[x] = dadosJson.equipamentos[i].Category;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.items2 = this.ti.filter(
          ta => ta.toLocaleLowerCase().includes(value2.toLocaleLowerCase())
        );

      }

    }else{
      // Se o campo input estiver vazio não exibe a recomendação
      if($tipo === 'Brand'){
        this.items = [];
      }else if($tipo === 'Category'){
        this.items2 = [];
      }

    }



  }

  selected(item, input): void {
    input.value = JSON.stringify(item);

    this.form.patchValue({selected: item});

    this.items = [];
  }


  selected2(item2, input): void {
    input.value = JSON.stringify(item2);

    this.form.patchValue({selected: item2});

    this.items2 = [];
  }

}
