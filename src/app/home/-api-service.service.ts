/* eslint-disable @typescript-eslint/quotes */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  /*private _fakeDatas = [
    {name: 'Chat', a:'b'},
    {name: 'Chien'},
    {name: 'Elephant'},
    {name: 'Girafe'},
    {name: 'Ourse'},
    {name: 'Oiseau'},
  ];*/

  private _categorias = [
    {categoria: ' Antena '},
    {categoria: 'Antena airgrid'},
    {categoria: 'Antena litebeam'},
    {categoria: 'Antena nanostation'},
    {categoria: 'Antena nanobeam'},
    {categoria: 'Antena wireless'},
    {categoria: 'Modem'},
    {categoria: 'Mini modem'},
    {categoria: 'Roteador'},
    {categoria: 'Acess point'},
    {categoria: 'Adaptador wireless usb'},
    {categoria: 'Adaptador pci express wireless'},
  ];

  private _brand = [
    {marca: 'ubiquite'},
    {marca: 'aquário'},
    {marca: 'intelbras'},
    {marca: 'microtik'},
    {marca: 'fiberhome'},
    {marca: 'vivo fibra'},
    {marca: 'zte'},
    {marca: 'd-link'},
    {marca: 'huawei'},
    {marca: 'tp-link'},
    {marca: 'multilaser'},
    {marca: 'tenda'},
    {marca: 'mercusys'},
    {marca: 'access point'},
    {marca: 'linksys'}
  ];

  private _model = [
    {model: "cpf-2620"},
     {model: "AG-HP-2G16"},
     {model: "AG-HP-2G20"},
     {model: "AG-HP-5G23"},
     {model: "AG-HP-5G27"},
     {model: "lbe-m5-23"},
     {model: "lbe-5ac-23"},
     {model: "lbe-5ac-gen2"},
     {model: "locoM9"},
     {model: "NSM2"},
     {model: "locoM2"},
     {model: "NSM3"},
     {model: "NSM365"},
     {model: "NSM5"},
     {model: "locoM5"},
     {model: "NBE-M2-13"},
     {model: "NBE-M5-16"},
     {model: "NBE-M5-19"},
     {model: "NBE-5AC-16"},
     {model: "NBE-5AC-Gen2"},
     {model: "mm5830dp"},
     {model: "APC 5A-15"},
     {model: "stx2"},
     {model: "SXT SA5"},
     {model: "SXT SA5 ac"},
     {model: "sxt lite 5"},
     {model: "SXT 6"},
     {model: "AN5506-01-A"},
     {model: "AN5506-02-B"},
     {model: "RTF8115VW"},
     {model: "Mf193"},
     {model: "Mf79U"},
     {model: "MF823"},
     {model: "dwm-157"},
    {model: "DWM-156"},
    {model: "DWR-910"},
    {model: "E303C"},
    {model: "E3272"},
    {model: "tl-wr740n"},
    {model: "TL-WR820N"},
    {model: "TL-WR829N"},
    {model: "TL-WR841HP"},
    {model: "TL-WR849N"},
    {model: "TL-WR940N"},
    {model: "TL-WR941HP"},
    {model: "TL-WR941ND"},
    {model: "TL-WR949N"},
    {model: "Archer C20"},
    {model: "Archer C50"},
    {model: "Archer C60"},
    {model: "Archer C7"},
    {model: "Deco M4"},
    {model: "Archer C2"},
    {model: "Archer C2300"},
    {model: "Archer C3150"},
    {model: "Deco M5"},
    {model: "Archer C5400"},
    {model: "DIR-600"},
    {model: "DIR-608"},
    {model: "DIR-610"},
    {model: "DIR-611"},
    {model: "DIR-615"},
    {model: "DIR-615 / X1"},
    {model: "DIR-615 / GF"},
    {model: "DIR-620"},
    {model: "DIR-300/A1"},
    {model: "DIR-628"},
    {model: "DIR-300 / NRU / B1"},
    {model: "DIR-632"},
    {model: "DIR-635"},
    {model: "DIR-636L"},
    {model: "DIR-645"},
    {model: "DIR-651"},
    {model: "DIR-655"},
    {model: "DIR-655 / B1"},
    {model: "DIR-685"},
    {model: "DIR-711"},
    {model: "DIR-815"},
    {model: "DIR-822"},
    {model: "DIR-825"},
    {model: "DIR-841"},
    {model: "DIR-841 / GF"},
    {model: "DIR-825 / GFRU"},
    {model: "DIR-842"},
    {model: "DIR-878"},
    {model: "DIR-882"},
    {model: "DIR-879"},
    {model: "DIR-1260"},
    {model: "DIR-2150"},
    {model: "DIR-330"},
    {model: "DIR-320"},
    {model: "DIR-300"},
    {model: "DIR-100"},
    {model: "DIR-120"},
    {model: "DIR-400"},
    {model: "DIR-457"},
    {model: "DIR-640L"},
    {model: "DIR-806A"},
    {model: "DIR-620S"},
    {model: "DSL-2740E"},
    {model: "DSL-245GR"},
    {model: "DLS-G2452GR"},
    {model: "DLS-2740U - RA"},
    {model: "DSL-224"},
    {model: "DSL-264OU / RB"},
    {model: "RE057"},
    {model: "RE047"},
    {model: "RE163"},
    {model: "RE011"},
    {model: "RE013"},
///
    {model: "RE074"},
    {model: "RE027"},
    {model: "RE019"},
    {model: "RE016"},
    {model: "RE072"},
    {model: "RE160"},
    {model: "RE014"},
    {model: "RE038"},
    {model: "RE751"},
    {model: "RE041"},
    {model: "RE170"},
    {model: "RE750"},
    {model: "RE073"},
    {model: "RE075"},
    {model: "RE570"},
    {model: "RE300"},
    {model: "RE060"},
    {model: "RE063"},
    {model: "RE185"},
    {model: "RE046"},
    {model: "RE015"},
    {model: "RE040"},
    {model: "RE017"},
    {model: "RE002"},
    {model: "RE006"},
    {model: "RE001"},
    {model: "RE039"},
    {model: "RE071"},
    {model: "RE024"},
    {model: "RE058"},
    {model: "RE171"},
    {model: "RE033"},
    {model: "RE160V"},
    {model: "RE184"},
    {model: "RE163V"},
    {model: "RE018"},
    {model: "RE707"},
    {model: "N301"},
    {model: "F3"},
    {model: "F6"},
    {model: "AC5V3.0"},
    {model: "AC6"},
    {model: "AC6 V5"},
    {model: "AC8"},
    {model: "AC10 V3"},
    {model: "AC10 "},
    {model: "AC23"},
    {model: "RX3"},
    {model: "RX9 PRO"},
    {model: "TX9 PRO"},
    {model: "AC10U V2"},
    {model: "MW301R"},
    {model: "MW305R"},
    {model: "MW325R"},
    {model: "MW330HP"},
    {model: "MR50G"},
    {model: "AC12G"},
    {model: "AC10"},
    {model: "WI-FORCE  W4 1200F"},
    {model: "TWIBI GIGA"},
    {model: "ACTION RF 1200"},
    {model: "GWI-FORCE GF 1200"},
    {model: "ACTION RG 1200"},
    {model: "WI-FORCE  W5 1200F"},
    {model: "RF301K"},
    {model: "IWR 3000N"},
    {model: "cpe-400"},
    {model: "WOM 5A-23 MIMO"},
    {model: "UAP‑AC-LITE"},
    {model: "LAP-120"},
    {model: "LAP-GPS"},
    {model: "UAP-Outdoor5"},
    {model: "UAP-Outdoor"},
    {model: "UAP‑AC‑LR"},
    {model: "UAP-AC-PRO"},
    {model: "EAP235-Wall"},
    {model: "EAP225"},
    {model: "EAP620 HD"},
    {model: "EAP245 v3"},
    {model: "EAP245 v1"},
    {model: "EAP115"},
    {model: "EAP110"},
    {model: "EAP225"},
    {model: "EAP225-Outdoor"},
    {model: "EAP235-Wall"},
    {model: "EAP225-Wall"},
    {model: "TL-WA7210N"},
    {model: "RE011"},
    {model: "RE031"},
    {model: "RE401"},
    {model: "RE402v"},
    {model: "RE002"},
    {model: "tl-wr840n w"},
    {model: "EC220-G5"},
    {model: "DWA-181"},
    {model: "DWA-131"},
    {model: "DWA-182"},
    {model: "DWA-171"},
    {model: "DWA-123"},
    {model: "Archer T3U"},
    {model: "Archer T2U Plus"},
    {model: "Archer T4U"},
    {model: "TL-WN823N"},
    {model: "TL-WN822N V1"},
    {model: "TL-WN822N (EU)V3"},
    {model: "TL-WN722N"},
    {model: "TL-WN821N"},
    {model: "TL-WN725N"},
    {model: "Archer T2U Nano"},
    {model: "RE035"},
    {model: "REO52"},
    {model: "RE044"},
    {model: "RE025"},
    {model: "Re034"},
    {model: "RE062"},
    {model: "IWA 3000"},
    {model: "IWA 3001"},
    {model: "Action A1200"},
    {model: "MW300UM"},
    {model: "MW150US"},
    {model: "U3"},
    {model: "U1"},
    {model: "U12"},
    {model: "U6"},
    {model: "U9"},
    {model: "AE1200"},
    {model: "AE2500"},
    {model: "AE6000"},
    {model: "WUSB6300"},
    {model: "T2UH"},
    {model: "TL-WN881ND"},
    {model: "TL-WN781ND"},
    {model: "TG-3468"},
    {model: "Archer T6E"},
    {model: "ARCHER T4E"},
    {model: "Archer T5E"},
    {model: "TX401"},
    {model: "Archer TX50E"},
    {model: "DWA-X582"},
    {model: "DGE-528T"},
    {model: "DWA-548"},
    {model: "DWA-582"}
  ];

  async getList() {
    // eslint-disable-next-line no-underscore-dangle
    return this._categorias;
  }

}
