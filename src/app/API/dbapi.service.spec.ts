import { TestBed } from '@angular/core/testing';

import { DBapiService } from './dbapi.service';

describe('DBapiService', () => {
  let service: DBapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DBapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
