import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DBapiService {
  headers: HttpHeaders;

  constructor( public http: HttpClient ) {
    this.headers = new HttpHeaders();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');


  }

  getdadosEquipamento(marca, modelo){
    return this.http.get('http://192.168.56.1/tcc_interface/db_mysql/get_dadosEquipamento.php?marca='+marca+'&modelo='+modelo);
  }

  getdadosCidade(nome){
    return this.http.get('http://192.168.56.1/tcc_interface/db_mysql/get_Cidade.php?nome='+nome);
  }

  getdadosTransmissao(meioTransmissao){
    return this.http.get('http://192.168.56.1/tcc_interface/db_mysql/get_meioTransmissao.php?meioTransmissao='+meioTransmissao);
  }

  addDadosForm(data) {
    return this.http.post('http://192.168.56.1/tcc_interface/db_mysql/insert_dadosForm.php', data);
  }


}
