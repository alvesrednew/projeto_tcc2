import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormControl } from '@angular/forms';
import { ApiService } from '../home/-api-service.service';
import { AlertController } from '@ionic/angular';
import { DBapiService } from '../API/dbapi.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage {
  cidade: any;
  periodoAcesso: any;
  velContratada: any;
  valPago: any;
  nomeProvedor: any;
  meioTransmissao: any;
  marcaEquip: any;
  categoriaEquip: any;
  modeloEquip: any;
  infoAdicionais: any;

  equipamento: any[] = [];
  equipamento2: any[] = [];
  equipamento3: any[] = [];
  provedor: any[] = [];
  cidadejs: any[] = [];
  transmissao: any[] = [];
  periodoAcessojs: any[] = [];

  dadosEquipamento: any[] = [];

  idEquipamento: any[] = [];
  idCidade: any[] = [];
  getmeioTransmissao: any[] = [];

  it: any[] = [];
  form: FormGroup = new FormGroup({
    selected: new FormControl(null)
  });


  handlerMessage = '';
  roleMessage = '';

  constructor(private route: Router, private _apiService: ApiService, public dbapiservice: DBapiService,
    private alertController: AlertController) {
    //this.getdadosEquipamento();
  }

  getdadosEquipamento(marca, modelo){
    //.toLowerCase() : Remove os ascentos e deixa as letras minusculas
   this.dbapiservice.getdadosEquipamento(marca.toLowerCase(), modelo.toLowerCase()).subscribe((res: any) => {
      //console.log('Sucess ***', res[0].id);


      this.idEquipamento[0] = res[0].id;

      //console.log(this.idEquipamento[0]);

    },(error: any) => {
      //alert('Error');
      console.log('Error ***', error);
    });




  }

  getdadosCidade(nome){
    //console.log(nome.toLowerCase());
    this.dbapiservice.getdadosCidade(nome.toLowerCase()).subscribe((res: any) =>{

      this.idCidade[0] = res[0].codCidade;
      //console.log(this.idCidade[0]);

    },(error: any) => {
      console.log('Error ***', error);
    });
  }

  getdadosTransmissao(nome){
    this.dbapiservice.getdadosTransmissao(nome.toLowerCase()).subscribe((res: any) =>{

      //console.log(res[0].meioTransmissao);

      this.getmeioTransmissao[0] = res[0].meioTransmissao;
      //console.log(this.getmeioTransmissao[0]);



    },(error: any) => {
      console.log('Error ***', error);
    });
  }


  verifInput() {
    if(this.cidade === undefined || this.periodoAcesso === undefined || this.velContratada === undefined || this.valPago === undefined ||
    this.nomeProvedor === undefined || this.meioTransmissao === undefined || this.categoriaEquip === undefined
    || this.marcaEquip === undefined || this.modeloEquip === undefined  ){
      alert('Todos os campos devem ser preenchidos');
    }

    else{
      this.getdadosEquipamento(this.marcaEquip, this.modeloEquip);
    this.getdadosCidade(this.cidade);
    this.getdadosTransmissao(this.meioTransmissao);
      this.presentAlert();
    }
  }

 insertdadosForm() {


    const data = {
      periodoAcesso: this.periodoAcesso,
      velContratada: this.velContratada,
      valPago: this.valPago,
      nomeProvedor: this.nomeProvedor,
      meioTransmissao: this.getmeioTransmissao[0],
      cidade: this.idCidade[0],
      idEquipamento: this.idEquipamento[0],
      infoAdicionais: this.infoAdicionais
    };

    console.log(JSON.stringify(data));



    this.dbapiservice.addDadosForm(JSON.stringify(data)).subscribe((res: any) => {
      console.log('Sucess ***', res);
      //this.route.navigate(['/form']);
      window.location.href = 'http://localhost:8100/form';

    },(error: any) => {
      //alert('Error');
      console.log('Error ***', error);
    });

  }

  forms(){


  }


  async presentAlert() {

    const alert = await this.alertController.create({
      header: 'Confirmar envio do formulário!',
      buttons: [
        {
          text: 'Cancel',
          //role: 'cancel',
          handler: () => {
            //this.handlerMessage = 'Alert canceled';
          },
        },
        {
          text: 'OK',
          role: 'confirm',
          handler: () => {
            //this.handlerMessage = 'Alert confirmed';
            this.insertdadosForm();
            //Recarrega pagina do formulário
            //window.location.href = 'http://localhost:8100/form';
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    this.roleMessage = `Dismissed with role: ${role}`;
  }


  async loadDados($var){

    if($var === 'Brand'){
      const response = await fetch('../../assets/data/equipamentos.json');
      //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

    else if($var === 'Category'){
      const response = await fetch('../../assets/data/equipamentos.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

    else if($var === 'Model'){
      const response = await fetch('../../assets/data/equipamentos.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

    else if($var === 'Provedor'){
      const response = await fetch('../../assets/data/provedor.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

    else if($var === 'Cidade'){
      const response = await fetch('../../assets/data/cidades.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

    else if($var === 'Transmissao'){
      const response = await fetch('../../assets/data/transmissao.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();
      //console.log(response);

      return dados;
    }

    else if($var === 'PeriodoAcesso'){
      const response = await fetch('../../assets/data/periodoAcesso.json');
    //const response = await fetch('../home/-api-service.service.spec.ts');

      const dados = await response.json();

      return dados;
    }

  }



  async inputChanged($event, $tipo){
    //$event.target.value: Caracteres digitados no input

    //Quando digitar um dado no input será feita o processo de buscar os dados para recomendação
    if($event.target.value !== ''){

      if($tipo === 'Brand'){
        const dadosJson = await this.loadDados($tipo);
        const value = $event.target.value;
        if (value.legth <= 0){
          this.equipamento = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.equipamentos.length -0; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.it.indexOf(dadosJson.equipamentos[i].Brand) < 0){

            this.it[x] =  dadosJson.equipamentos[i].Brand;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.equipamento = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        );


          this.it = [];
      }

      else if($tipo === 'Category'){
        const dadosJson = await this.loadDados($tipo);

        const value2 = $event.target.value;
        if (value2.legth <= 0){
          this.equipamento2 = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.equipamentos.length -0; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.it.indexOf(dadosJson.equipamentos[i].Category) < 0){
            //console.log(users.equipamentos[i].Category);
            this.it[x] = dadosJson.equipamentos[i].Category;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.equipamento2 = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value2.toLocaleLowerCase())
        );

        this.it = [];

      }

      else if($tipo === 'Model'){
        const dadosJson = await this.loadDados($tipo);

        const value3 = $event.target.value;
        if (value3.legth <= 0){
          this.equipamento3 = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.equipamentos.length -0; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.it.indexOf(dadosJson.equipamentos[i].Model) < 0){
            //console.log(users.equipamentos[i].Category);
            this.it[x] = dadosJson.equipamentos[i].Model;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.equipamento3 = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value3.toLocaleLowerCase())
        );

        this.it = [];

      }


      else if($tipo === 'Provedor'){
        const dadosJson = await this.loadDados($tipo);

        const value4 = $event.target.value;
        if (value4.legth <= 0){
          this.provedor = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.provedor.length -0; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.it.indexOf(dadosJson.provedor[i].nome) < 0){
            //console.log(users.equipamentos[i].Category);
            this.it[x] = dadosJson.provedor[i].nome;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.provedor = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value4.toLocaleLowerCase())
        );

        this.it = [];

      }

      else if($tipo === 'Transmissao'){
        const dadosJson = await this.loadDados($tipo);

        const value5 = $event.target.value;
        if (value5.legth <= 0){
          this.transmissao = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.transmissao.length -0; i++){
          /*Verifica se o array items já contém o dado na posição i.
            Caso não possua, o seguinte dado sera inserido em items
          */
          if(this.it.indexOf(dadosJson.transmissao[i].tipo) < 0){
            //console.log(users.equipamentos[i].Category);
            this.it[x] = dadosJson.transmissao[i].tipo;

            x = x + 1;
          }

        }

        //console.log(this.items[0]);
        this.transmissao = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value5.toLocaleLowerCase())
        );


      }

      else if($tipo === 'Cidade'){
          const dadosJson = await this.loadDados($tipo);

          const value6 = $event.target.value;
          if (value6.legth <= 0){
            this.cidadejs = [];
            return;
          }

          let x = 0;
          //
          for(let i = 0; i < dadosJson.cidades.length -0; i++){
            /*Verifica se o array items já contém o dado na posição i.
              Caso não possua, o seguinte dado sera inserido em items
            */
            if(this.it.indexOf(dadosJson.cidades[i].nome) < 0){
              //console.log(users.equipamentos[i].Category);
              this.it[x] = dadosJson.cidades[i].nome;

              x = x + 1;
            }
          }


          //console.log(this.items[0]);
          this.cidadejs = this.it.filter(
            ta => ta.toLocaleLowerCase().includes(value6.toLocaleLowerCase())
          );

      }

      else if($tipo === 'PeriodoAcesso'){
        /*
        const dadosJson = await this.loadDados($tipo);

        const value7 = $event.target.value;
        if (value7.legth <= 0){
          this.periodoAcessojs = [];
          return;
        }

        let x = 0;
        //
        for(let i = 0; i < dadosJson.acessointernet.length -0; i++){

          if(this.it.indexOf(dadosJson.acessointernet[i].ano) < 0){
            //console.log(users.equipamentos[i].Category);
            this.it[x] = dadosJson.acessointernet[i].ano;

            x = x + 1;
          }
        }


        //console.log(this.items[0]);
        this.periodoAcessojs = this.it.filter(
          ta => ta.toLocaleLowerCase().includes(value7.toLocaleLowerCase())
        );
        */
    }

    }else{
      // Se o campo input estiver vazio não exibe a recomendação
      if($tipo === 'Brand'){
        this.equipamento = [];
      }
      else if($tipo === 'Category'){
        this.equipamento2 = [];
      }
      else if($tipo === 'Model'){
        this.equipamento3 = [];
      }
      else if($tipo === 'Provedor'){
        this.provedor = [];
      }
      else if($tipo === 'Transmissao'){
        this.transmissao = [];
      }
      else if($tipo === 'Cidade'){
        this.cidadejs = [];
      }
      else if($tipo === 'PeriodoAcesso'){
        this.periodoAcessojs = [];
      }


    }



  }

  // Seleciona o dado escolhido na recomendação
  selected(item, input, $tipo): void {
    if($tipo === 'Category'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.equipamento2 = [];

    }else if($tipo === 'Brand'){
      //input.value = JSON.stringify(item);
      input.value = item;

      this.form.patchValue({selected: item});

      this.equipamento = [];
    }else if($tipo === 'Brand'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.equipamento = [];
    }else if($tipo === 'Model'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.equipamento3 = [];
    }else if($tipo === 'Cidade'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.cidadejs = [];
    }else if($tipo === 'Transmissao'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.transmissao = [];
    }else if($tipo === 'PeriodoAcesso'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.periodoAcessojs = [];
    }else if($tipo === 'Provedor'){
      input.value = item;

      this.form.patchValue({selected: item});

      this.provedor = [];
    }

  }


}
